/**
* @file     print_elementos.h
* @brief    Implementacao da funcao generica que faz a impressao de elementos com um separador e um rotulo
* @author   Pedro Emerick (p.emerick@live.com)
* @since    05/06/2017
* @date	    07/06/2017
*/

#ifndef PRINT_ELEMENTOS_H
#define PRINT_ELEMENTOS_H

#include <iostream>
using std::cout;
using std::endl;

/** 
 * @brief	Função genérica que faz a impressao dos elementos passados 
 *          com um rotulo e um separador entre os elementos
 * @param 	collection Dados para impressao
 * @param 	label Rotulo para ser impresso antes dos elementos, tem por padrao vazio
 * @param	separator Separador dos elementos na impressao, tem por padrao espaço em branco
 */
template<typename TContainer>
void print_elementos (const TContainer& collection, const char* label = "", const char separator = ' ')
{
    cout << label;

    for (auto it = collection.begin (); it != collection.end (); it++) {
        if (*collection.end () == *it)
            cout << (*it) << endl;
        else 
            cout << (*it) << separator;
    }
}

#endif