/**
* @file     closest2mean.h
* @brief    Implementacao da funcao generica que retorna o iterador para o elemento mais perto da media
* @author   Pedro Emerick (p.emerick@live.com)
* @since    05/06/2017
* @date	    07/06/2017
*/

#ifndef CLOSEST2MEAN_H
#define CLOSEST2MEAN_H

#include <numeric>
using std::accumulate;

/** 
 * @brief	Função genérica que calcula a media de um conjunto de numeros 
 *          e verifica qual o numero entre eles é o mais proximo da media 
 * @param 	first Iterador para o inicio
 * @param	last Iterador para o fim
 * @return  Iterador do elemento mais proximo da media
 */
template<typename InputIterator>
InputIterator closest2mean (InputIterator first, InputIterator last)
{
    int tamanho = 0;

    for (auto it = first; it != last; it++) {
        tamanho ++;
    }

    float media = (accumulate (first, last, 0)) / tamanho;

    float proximo = media - (*first);
    if (proximo < 0) proximo *= -1;
    auto result = first;

    cout << "Media: " << media << endl;

    for (auto it = first; it != last; it++) {
        
        float temp = media - (*it);
    
        if (temp < 0)
            temp *= -1;

        if (temp < proximo) {
            proximo = temp;
            result = it;
        }
    }

    return result;
}

#endif