/**
* @file   pessoa.h
* @brief  Arquivo com os atributos e metodos da classe Pessoa
* @author Pedro Emerick (p.emerick@live.com)
* @since  05/06/2017
* @date   08/06/2017
*/

#ifndef PESSOA_H
#define PESSOA_H

#include <string>
using std::string;

#include <ostream>
using std::ostream;

/**
 * @class   Pessoa pessoa.h
 * @brief   Classe que representa uma pessoa fisica
 * @details Os atributos de uma pessoa são o nome, cpf e idade
 */ 
class Pessoa {
    protected:
        string nome;                /**< Nome da pessoa */
        long int cpf;               /**< Cpf da pessoa */
        int idade;                  /**< Idade da pessoa */
    public:
        /** @brief Construtor padrao */
        Pessoa ();

        /** @brief Construtor parametrizado */
        Pessoa (string n, long int c, int i);

        /** @brief Retorna o cpf da pessoa */
        long int getCpf ();

        /** @brief Modifica o cpf da pessoa */
        void setCpf (long int c);

        /** @brief Modifica o nome da pessoa */
        void setNome (string n);

        /** @brief Modifica a idade da pessoa */
        void setIdade (int i);

        /** @brief Sobrecarga do operador de insercao em stream */
        friend ostream& operator << (ostream &os, Pessoa const &p);

        /** @brief Retorna uma string com todos os dados da pessoa */
        string To_string ();

        /** @brief Destrutor padrao */
        ~Pessoa ();
};

#endif