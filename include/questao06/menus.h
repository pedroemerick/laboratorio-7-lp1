/**
* @file     menus.h
* @brief    Declaracao dos prototipos das funcoes que imprimem os menus do programa e recebe a opcao do usuario
* @author   Pedro Emerick (p.emerick@live.com)
* @since    11/05/2017
* @date	    08/06/2017
*/

#ifndef MENUS_H
#define MENUS_H

/**
* @brief Função que imprime o menu principal com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_principal ();

/**
* @brief Função que imprime o menu das contas com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_conta ();

#endif