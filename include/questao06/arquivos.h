/**
* @file     arquivos.h
* @brief    Declaracao dos prototipos das funcoes que manipulam os arquivos de entrada e saida de dados
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    08/06/2017
*/

#ifndef ARQUIVOS_H
#define ARQUIVOS_H

#include "conta.h"

#include <fstream>
using std::ofstream;
using std::ifstream;

#include <list>
using std::list;

/**
* @brief Função que salva as contas em um arquivo .csv
* @param arquivo Arquivo para gravação dos dados
* @param contas Lista com todos as contas
* @param qtd_contas Quantidade de contas
*/
void arquivo_saida_contas (ofstream &arquivo, list <Conta*> &contas, int &qtd_contas);

/**
* @brief Função que carrega as contas de um arquivo .csv
* @param arquivo Arquivo para leitura dos dados
* @param contas Lista com todos as contas
* @param qtd_contas Quantidade de contas
*/
void carrega_contas (ifstream &arquivo, list <Conta*> &contas, int &qtd_contas);

#endif