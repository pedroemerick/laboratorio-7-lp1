/**
* @file     funcoes_q6.h
* @brief    Declaracao do prototipo da funcao que realiza a abertura de uma conta, fecha uma conta,
*           verifica se uma conta existe, faz acesso a uma conta, e retorna uma conta da lista
* @author   Pedro Emerick (p.emerick@live.com)
* @since    05/06/2017
* @date	    08/06/2017
*/

#ifndef FUNCOES_Q6_H
#define FUNCOES_Q6_H

#include "conta_corrente.h"
#include "conta_poupanca.h"
#include "menus.h"
#include "data.h"

#include <list>
using std::list;

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

/** 
 * @brief	Função que verifica se existe uma conta na lista de contas atraves do cpf
 * @param 	contas Lista com as contas
 * @param	cpf Cpf do titular da conta
 * @return  Se a conta existe ou nao na lista
 */
bool existe_conta (list <Conta*> &contas, long int cpf);

/** 
 * @brief	Função que retorna uma conta da lista de contas atraves do cpf
 * @param 	contas Lista com as contas
 * @param	cpf Cpf do titular da conta
 * @return  Iterador para a conta desejada
 */
list<Conta*>::iterator retorna_conta (list <Conta*> &contas, long int cpf);

/** 
 * @brief	Função que faz a abertura de uma conta, recebendo os dados necessarios do usuario e 
 *          inserindo na lista de contas
 * @param 	contas Lista com as contas
 * @param	opcao Opcao de qual tipo de conta sera aberta, se sera corrente ou poupanca
 * @param   qtd_contas Quantidade de contas
 */
void abrir_conta (list <Conta*> &contas, int opcao, int &qtd_contas);

/** 
 * @brief	Função que faz acesso a uma conta e faz as alteracoes possiveis e desejadas pelo usuario
 * @param 	contas Lista com as contas
 */
void acesso_conta (list <Conta*> &contas);

/** 
 * @brief	Função que faz o fechamento de uma conta removendo-o da lista
 * @param 	contas Lista com as contas
 * @param   qtd_contas Quantidade de contas
 */
void remove_conta (list <Conta*> &contas, int &qtd_contas);

#endif