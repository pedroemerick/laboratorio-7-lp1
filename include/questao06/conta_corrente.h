/**
* @file   conta_corrente.h
* @brief  Arquivo com os atributos e metodos da classe ContaCorrente
* @author Pedro Emerick (p.emerick@live.com)
* @since  05/06/2017
* @date   08/06/2017
*/

#ifndef CONTA_CORRENTE_H
#define CONTA_CORRENTE_H

#include "conta.h"
#include "pessoa.h"
#include "data.h"

#include <string>
using std::string;

#include <ostream>
using std::ostream;

/**
 * @class   ContaCorrente conta_corrente.h
 * @brief   Classe que representa uma pessoa conta corrente
 * @details Os atributos de uma conta corrente são o saldo, limite, juros, juros positivos, juros negativos, 
            e a uma data
 */ 
class ContaCorrente : public Conta {
    private:
        float limite;                   /**< Limite da conta */
        Data data;                      /**< Data que foi aplicado o juros pela ultima vez */
    public:
        /** @brief Construtor padrao */
        ContaCorrente ();
        
        /** @brief Construtor parametrizado */
        ContaCorrente (string n, long int c, int i, float l, float j, Data cr, float jn, float jp, float s, int av, string t);
        
        /** @brief Realiza um deposito na conta */
        void Deposito ();
        
        /** @brief Realiza um saque na conta */
        void Saque ();
        
        /** @brief Retorna o saldo da conta */
        float Saldo ();
        
        /** @brief Retorna o juros positivo da conta */
        float JurosPositivos ();
        
        /** @brief Retorna o juros negativo da conta */
        float JurosNegativos ();
        
        /** @brief Atualiza o saldo da conta de acordo com o juros */
        void Atualiza ();

        /** @brief Avanca o numero de dias desejado pelo usuario na data */
        void AvancaData (int ad);

         /** @brief Retorna o titular da conta */
        Pessoa getTitular ();

        /** @brief Retorna o tipo da conta */
        string getTipo ();

        /** @brief Sobrecarga do operador de insercao em stream */
        friend ostream& operator << (ostream &os, ContaCorrente const &cc );

        /** @brief Retorna uma string com todos os dados da conta */
        string To_string ();

        /** @brief Destrutor padrao */
        ~ContaCorrente ();
};

#endif