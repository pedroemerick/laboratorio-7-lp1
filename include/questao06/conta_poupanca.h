/**
* @file   conta_poupanca.h
* @brief  Arquivo com os atributos e metodos da classe ContaPoupanca
* @author Pedro Emerick (p.emerick@live.com)
* @since  05/06/2017
* @date   08/06/2017
*/

#ifndef CONTA_POUPANCA_H
#define CONTA_POUPANCA_H

#include "conta.h"
#include "pessoa.h"
#include "data.h"

#include <string>
using std::string;

#include <ostream>
using std::ostream;

/**
 * @class   ContaPoupanca conta_poupanca.h
 * @brief   Classe que representa uma pessoa conta poupanca
 * @details Os atributos de uma conta corrente são o saldo, saldo na data de aniversario, 
 *          juros, juros positivos, juros negativos, e a data de aniversario
 */
class ContaPoupanca : public Conta {
    private:
        float saldo_aniversario;            /**< Saldo no aniversario da conta */
        Data aniversario;                   /**< Data de aniversario da conta */
    public:
        /** @brief Construtor padrao */
        ContaPoupanca ();

        /** @brief Construtor parametrizado */
        ContaPoupanca (string n, long int c, int i, Data a, float j, float jn, float jp, float s, int av, string t, float sa);
        
        /** @brief Realiza um deposito na conta */
        void Deposito ();
        
        /** @brief Realiza um saque na conta */
        void Saque ();
        
        /** @brief Retorna o saldo da conta */
        float Saldo ();
        
        /** @brief Retorna o juros positivo da conta */
        float JurosPositivos ();
        
        /** @brief Retorna o juros negativo da conta */
        float JurosNegativos ();
        
        /** @brief Atualiza o saldo da conta de acordo com o juros */
        void Atualiza ();

        /** @brief Avanca o numero de dias desejado pelo usuario na data */
        void AvancaData (int ad);

        /** @brief Retorna o titular da conta */
        Pessoa getTitular ();

        /** @brief Retorna o tipo da conta */
        string getTipo ();

        /** @brief Sobrecarga do operador de insercao em stream */
        friend ostream& operator << (ostream &os, ContaPoupanca const &cp);

        /** @brief Retorna uma string com todos os dados da conta */
        string To_string ();

        /** @brief Destrutor padrao */
        ~ContaPoupanca ();
};

#endif