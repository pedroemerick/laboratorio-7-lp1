/**
* @file   conta.h
* @brief  Arquivo com os atributos e metodos da interface Conta
* @author Pedro Emerick (p.emerick@live.com)
* @since  05/06/2017
* @date   08/06/2017
*/

#ifndef CONTA_H
#define CONTA_H

#include "data.h"
#include "pessoa.h"

#include <string>
using std::string;

/**
 * @class   Conta conta.h
 * @brief   Classe que representa uma conta
 * @details Os atributos de uma conta sao o tipo, o titular, o saldo, 
 *          juros positivos e negativos, porcentagem de juros, e o avanco na data
 */ 
class Conta {
    protected:
        string tipo;                    /**< Tipo da conta */
        Pessoa titular;                 /**< Titular da conta */
        float saldo;                    /**< Saldo da conta */
        float juros_positivos;          /**< Juros Positivos da conta */
        float juros_negativos;          /**< Juros Negativos da conta */
        int avanco;                     /**< Dias avancados na conta */
        float juros;                    /**< Porcentagem de juros da conta */
    public:
        /** 
         * @brief   Construtor padrao 
         * @details Os valores numericos sao inicializados com zero
         */
        Conta () {
            saldo = 0;
            juros_positivos = 0;
            juros_negativos = 0;
            avanco = 0;
            juros = 0;
        }

        /** @brief Metodo virtual puro que deve realizar um deposito na conta */
        virtual void Deposito () = 0;

        /** @brief Metodo virtual puro que deve realizar um saque na conta */
        virtual void Saque () = 0;

        /** @brief Metodo virtual puro que deve retornar o saldo da conta */
        virtual float Saldo () = 0;

        /** @brief Metodo virtual puro que deve retornar o juros positivo da conta */
        virtual float JurosPositivos () = 0;

         /** @brief Metodo virtual puro que deve retornar o juros negativo da conta */
        virtual float JurosNegativos () = 0;
        
        /** @brief Metodo virtual puro que deve atualizar o saldo da conta de acordo com o juros */
        virtual void Atualiza () = 0;
        
        /** @brief Metodo virtual puro que deve avancar o numero de dias desejado pelo usuario na data */
        virtual void AvancaData (int ad) = 0;
        
        /** @brief Metodo virtual puro que deve retornar o titular da conta */
        virtual Pessoa getTitular () = 0;
        
        /** @brief Metodo virtual puro que deve retornar o tipo da conta */
        virtual string getTipo () = 0;

        /** @brief Metodo virtual puro que deve retornar uma string com todos os dados da conta */
        virtual string To_string () = 0;

        /** 
         * @brief   Destrutor padrao 
         * @details Não ocorre nada no destrutor, pois nao tem variaveis para serem destruidas
         */
        ~Conta () {

        }
};  

#endif
        