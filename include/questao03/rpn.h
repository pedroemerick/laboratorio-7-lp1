/**
* @file     rpn.h
* @brief    Declaracao do prototipo da funcao que resolve uma rpn (Notação Polonesa Inversa)
* @author   Pedro Emerick (p.emerick@live.com)
* @since    05/06/2017
* @date	    07/06/2017
*/

#ifndef RPN_H
#define RPN_H

#include <stack>
using std::stack;

/** 
 * @brief	Função faz a resolucao de uma rpn utilizando pilhas
 * @param 	pilha Pilha com os dados da rpn (numeros e operandos)
 * @param	pilha_aux Pilha para auxiliar na resolucao da rpn, armazenando os numeros com os resultados
 */
void resolve_rpn (stack <char*> &pilha, stack <int> &pilha_aux);

#endif