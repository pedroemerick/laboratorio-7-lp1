LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc
TEST_DIR = ./test

CC = g++
CPPFLAGS = -Wall -pedantic -ansi -std=c++11

RM = rm -rf
RM_TUDO = rm -fr

.PHONY: all clean debug doc doxygen valgrind1

all: init questao01 questao02 questao03 questao04 questao06

debug: CPPFLAGS += -g -O0
debug: all

valgrind1: 
	valgrind --leak-check=full --show-reachable=yes -v ./bin/closest2mean

init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/questao01/
	@mkdir -p $(OBJ_DIR)/questao02/
	@mkdir -p $(OBJ_DIR)/questao03/
	@mkdir -p $(OBJ_DIR)/questao04/
	@mkdir -p $(OBJ_DIR)/questao05/
	@mkdir -p $(OBJ_DIR)/questao06/

# QUESTAO 01

questao01: CPPFLAGS += -I. -I$(INC_DIR)/questao01
questao01: $(OBJ_DIR)/questao01/main1.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/closest2mean $^
	@echo "*** [Executavel closest2mean criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao01/main1.o: $(SRC_DIR)/questao01/main1.cpp $(INC_DIR)/questao01/closest2mean.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# QUESTAO 02

questao02: CPPFLAGS += -I. -I$(INC_DIR)/questao02
questao02: $(OBJ_DIR)/questao02/main2.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/print_elementos $^
	@echo "*** [Executavel print_elementos criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao02/main2.o: $(SRC_DIR)/questao02/main2.cpp $(INC_DIR)/questao02/print_elementos.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# QUESTAO 03

questao03: CPPFLAGS += -I. -I$(INC_DIR)/questao03
questao03: $(OBJ_DIR)/questao03/main3.o $(OBJ_DIR)/questao03/rpn.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/rpn $^
	@echo "*** [Executavel rpn criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao03/main3.o: $(SRC_DIR)/questao03/main3.cpp $(INC_DIR)/questao03/rpn.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao03/rpn.o: $(SRC_DIR)/questao03/rpn.cpp $(INC_DIR)/questao03/rpn.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# QUESTAO 04

questao04: CPPFLAGS += -I. -I$(INC_DIR)/questao04
questao04: $(OBJ_DIR)/questao04/main4.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/showprimos $^
	@echo "*** [Executavel showprimos criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao04/main4.o: $(SRC_DIR)/questao04/main4.cpp
	$(CC) -c $(CPPFLAGS) -o $@ $<

# QUESTAO 05

questao05: CPPFLAGS += -I. -I$(INC_DIR)/questao05
questao05: $(OBJ_DIR)/questao05/main5.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/square $^
	@echo "*** [Executavel square criado em $(BIN_DIR)] ***"
	@echo "====================================================="

# Alvo para a construção do 'main5.o':
$(OBJ_DIR)/questao05/main5.o: $(SRC_DIR)/questao05/main5.cpp
	$(CC) -c $(CPPFLAGS) -o $@ $<

# QUESTAO 06

questao06: CPPFLAGS += -I. -I$(INC_DIR)/questao06
questao06: $(OBJ_DIR)/questao06/main6.o $(OBJ_DIR)/questao06/conta_corrente.o $(OBJ_DIR)/questao06/conta_poupanca.o $(OBJ_DIR)/questao06/pessoa.o $(OBJ_DIR)/questao06/data.o $(OBJ_DIR)/questao06/menus.o $(OBJ_DIR)/questao06/funcoes_q6.o $(OBJ_DIR)/questao06/arquivos.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/banco $^
	@echo "*** [Executavel banco criado em $(BIN_DIR)] ***"
	@echo "====================================================="

# Alvo para a construção do 'main6.o':
$(OBJ_DIR)/questao06/main6.o: $(SRC_DIR)/questao06/main6.cpp $(INC_DIR)/questao06/conta_corrente.h $(INC_DIR)/questao06/conta_poupanca.h $(INC_DIR)/questao06/menus.h $(INC_DIR)/questao06/funcoes_q6.h $(INC_DIR)/questao06/arquivos.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# Alvo para a construção do 'conta_corrente.o':
$(OBJ_DIR)/questao06/conta_corrente.o: $(SRC_DIR)/questao06/conta_corrente.cpp $(INC_DIR)/questao06/conta_corrente.h $(INC_DIR)/questao06/conta.h  $(INC_DIR)/questao06/pessoa.h $(INC_DIR)/questao06/data.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# Alvo para a construção do 'conta_poupanca.o':
$(OBJ_DIR)/questao06/conta_poupanca.o: $(SRC_DIR)/questao06/conta_poupanca.cpp $(INC_DIR)/questao06/conta_poupanca.h $(INC_DIR)/questao06/conta.h $(INC_DIR)/questao06/pessoa.h $(INC_DIR)/questao06/data.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# Alvo para a construção do 'pessoa.o':
$(OBJ_DIR)/questao06/pessoa.o: $(SRC_DIR)/questao06/pessoa.cpp $(INC_DIR)/questao06/pessoa.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# Alvo para a construção do 'data.o':
$(OBJ_DIR)/questao06/data.o: $(SRC_DIR)/questao06/data.cpp $(INC_DIR)/questao06/data.h $(INC_DIR)/questao06/menus.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# Alvo para a construção do 'menus.o':
$(OBJ_DIR)/questao06/menus.o: $(SRC_DIR)/questao06/menus.cpp $(INC_DIR)/questao06/menus.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# Alvo para a construção do 'funcoes_q6.o':
$(OBJ_DIR)/questao06/funcoes_q6.o: $(SRC_DIR)/questao06/funcoes_q6.cpp $(INC_DIR)/questao06/funcoes_q6.h $(INC_DIR)/questao06/menus.h $(INC_DIR)/questao06/data.h $(INC_DIR)/questao06/conta_poupanca.h $(INC_DIR)/questao06/conta_corrente.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# Alvo para a construção do 'arquivos.o':
$(OBJ_DIR)/questao06/arquivos.o: $(SRC_DIR)/questao06/arquivos.cpp $(INC_DIR)/questao06/arquivos.h $(INC_DIR)/questao06/conta_poupanca.h $(INC_DIR)/questao06/conta_corrente.h $(INC_DIR)/questao06/conta.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

doxygen:
	doxygen -g

doc:
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM_TUDO) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile

clean:
	@echo "====================================================="
	@echo "Limpando pasta $(BIN_DIR), $(OBJ_DIR) e $(IMG_DIR)"
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/questao01/*
	$(RM) $(OBJ_DIR)/questao02/*
	$(RM) $(OBJ_DIR)/questao03/*
	$(RM) $(OBJ_DIR)/questao04/*
	$(RM) $(OBJ_DIR)/questao05/*
	$(RM) $(OBJ_DIR)/questao06/*
