/**
* @file	    main3.cpp
* @brief	Arquivo com a função principal do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since	05/06/2017
* @date	    07/06/2017
*/

#include "rpn.h"

#include <stack>
using std::stack;

#include <iostream>
using std::cout;
using std::endl;

/**
* @brief Função principal do programa
* @param argc Variavel que contem a quantidade de argumentos passados via do terminal
* @param *argv[] Vetor que contem os argumentos passados via do terminal
*/
int main (int argc, char* argv[])
{
    stack <char*> pilha;
    stack <int> pilha_aux;

    // Pega os dados passados via terminal e armazena em uma pilha, com a ordem inversa
    for (int ii = argc-1; ii > 0; ii--) {
        pilha.push (argv [ii]);
    }

    resolve_rpn (pilha, pilha_aux);

    cout << "Resultado da RPN: " << pilha_aux.top () << endl;

    return 0;
}