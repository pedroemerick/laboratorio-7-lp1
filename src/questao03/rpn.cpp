/**
* @file     rpn.cpp
* @brief    Implementacao da funcao que resolve uma rpn (Notação Polonesa Inversa)
* @author   Pedro Emerick (p.emerick@live.com)
* @since    05/06/2017
* @date	    07/06/2017
*/

#include "rpn.h"

#include <stack>
using std::stack;

#include <cstdlib>
using std::atoi;

#include <string>
using std::string;
using std::stoi;

/** 
 * @brief	Função faz a resolucao de uma rpn utilizando pilhas
 * @param 	pilha Pilha com os dados da rpn (numeros e operandos)
 * @param	pilha_aux Pilha para auxiliar na resolucao da rpn, armazenando os numeros com os resultados
 */
void resolve_rpn (stack <char*> &pilha, stack <int> &pilha_aux)
{
    int operando1 = 0;
    int operando2 = 0;

    while(!pilha.empty ())
    {
        // Le o caracter
        string lido (pilha.top());
        pilha.pop ();

        // Verifica se é algum simbolo de operacao, para resolver a respectiva operacao
        // Armazena o resultado da operacao na pilha auxiliar
        if (lido == "+") {
            operando2 = pilha_aux.top ();
            pilha_aux.pop();
            operando1= pilha_aux.top();
            pilha_aux.pop ();

            pilha_aux.push (operando1 + operando2);
        }
        else if (lido == "-") {
            operando2 = pilha_aux.top ();
            pilha_aux.pop();
            operando1= pilha_aux.top();
            pilha_aux.pop ();

            pilha_aux.push (operando1 - operando2);
        }
        else if (lido == "/") {
            operando2 = pilha_aux.top ();
            pilha_aux.pop();
            operando1= pilha_aux.top();
            pilha_aux.pop ();

            pilha_aux.push (operando1 / operando2);
        }
        else if (lido == "x") {
            operando2 = pilha_aux.top ();
            pilha_aux.pop();
            operando1= pilha_aux.top();
            pilha_aux.pop ();

            pilha_aux.push (operando1 * operando2);
        }
        else {
            // Se for numero, armazena na pilha auxiliar
            pilha_aux.push (stoi(lido));   
        }
    }
}