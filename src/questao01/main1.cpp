/**
* @file	    main1.cpp
* @brief	Arquivo com a função principal do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since	05/06/2017
* @date	    07/06/2017
*/

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

#include "closest2mean.h"

/**
* @brief Função principal do programa
*/
int main() 
{
    vector<int> v { 1, 2, 3, 30, 40, 50 };

    auto result = closest2mean(v.begin(), v.end());
    cout << "Valor mais próximo da media: " << (*result) << endl;

    return 0;
}