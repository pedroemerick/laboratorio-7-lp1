/**
* @file	    main2.cpp
* @brief	Arquivo com a função principal do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since	05/06/2017
* @date	    07/06/2017
*/

#include <set>
using std::set;

#include "print_elementos.h"

/**
* @brief Função principal do programa
*/
int main() 
{
    set<int> numeros;

    numeros.insert(3);
    numeros.insert(1);
    numeros.insert(4);
    numeros.insert(1);
    numeros.insert(2);
    numeros.insert(5);
    print_elementos(numeros, "Set: ");
    print_elementos(numeros, "CSV Set: ", ';');

    return 0;
}