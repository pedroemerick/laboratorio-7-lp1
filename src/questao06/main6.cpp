/**
* @file	    main6.cpp
* @brief	Arquivo com a função principal do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since	05/06/2017
* @date	    08/06/2017
*/

#include "conta_corrente.h"
#include "conta_poupanca.h"
#include "menus.h"
#include "data.h"
#include "funcoes_q6.h"
#include "arquivos.h"

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <string>
using std::string;

#include <list>
using std::list;

#include <sstream>
using std::ws;

#include <fstream>
using std::ofstream;
using std::ifstream;

/**
* @brief Função principal do programa
*/
int main () 
{
    list <Conta*> contas;
    int qtd_contas = 0;

    ifstream arquivo_carrega_contas ("./data/contas.csv");
    if (arquivo_carrega_contas) {
        carrega_contas (arquivo_carrega_contas, contas, qtd_contas);
        arquivo_carrega_contas.close ();        
    }

    int opcao = -1;
    while (opcao != 0) 
    {
        opcao = menu_principal ();

        switch (opcao)
        {
            case 1: 
                abrir_conta (contas, opcao, qtd_contas);
                break;
            case 2:
                abrir_conta (contas, opcao, qtd_contas);
                break;
            case 3: 
                acesso_conta (contas);
                break;
            case 4:
                remove_conta (contas, qtd_contas);
                break;
            case 0:
                break;
            default:
                cout << "Opcao invalida !!!" << endl;
                break;
        }
    }

    ofstream arquivo_contas ("./data/contas.csv");
    arquivo_saida_contas (arquivo_contas, contas, qtd_contas);
    arquivo_contas.close ();

    return 0;
}