/**
* @file     arquivos.cpp
* @brief    Implementacao das funcoes que manipulam os arquivos de entrada e saida de dados
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/05/2017
* @date	    08/06/2017
*/

#include "arquivos.h"
#include "conta_corrente.h"
#include "conta_poupanca.h"
#include "conta.h"

#include <fstream>
using std::ofstream;
using std::ifstream;

#include <iostream>
using std::endl;

#include <cstdlib>
using std::atoi;
using std::atof;
using std::atol;

#include <ctime>

#include <sstream>
using std::ostringstream;

#include <iomanip>

#include <list>
using std::list;

/**
* @brief Função que salva as contas em um arquivo .csv
* @param arquivo Arquivo para gravação dos dados
* @param contas Lista com todos as contas
* @param qtd_contas Quantidade de contas
*/
void arquivo_saida_contas (ofstream &arquivo, list <Conta*> &contas, int &qtd_contas)
{
    string dados_contas;

    // Escreve a quantidade de produtos no arquivo
    arquivo << qtd_contas << endl;

    // Percorre a lista de produtos acrescentando todos os dados dos produtos em uma string
    for (auto it = contas.begin (); it != contas.end (); it++)
    {
        dados_contas += (*it)->To_string ();
    }

    // Escreve todos os produtos com seus dados no arquivo
    arquivo << dados_contas;
}

/**
* @brief Função que carrega as contas de um arquivo .csv
* @param arquivo Arquivo para leitura dos dados
* @param contas Lista com todos as contas
* @param qtd_contas Quantidade de contas
*/
void carrega_contas (ifstream &arquivo, list <Conta*> &contas, int &qtd_contas)
{
    string temp;
    int qtd_temp;

    getline (arquivo, temp);
    qtd_temp = atoi (temp.c_str ());
    qtd_contas += atoi (temp.c_str ());

    for (int ii = 0; ii < qtd_temp; ii++) 
    {
        string tipo;
        string nome;
        long int cpf;
        int idade;
        float saldo;
        float juros_positivos;
        float juros_negativos;
        int avanco;
        float juros;

        getline (arquivo, tipo, ';');
        getline (arquivo, nome, ';');
        getline (arquivo, temp, ';');
        cpf = atol (temp.c_str());
        getline (arquivo, temp, ';');
        idade = atoi (temp.c_str());
        getline (arquivo, temp, ';');
        saldo = atof (temp.c_str());
        getline (arquivo, temp, ';');
        juros_positivos = atof (temp.c_str());
        getline (arquivo, temp, ';');
        juros_negativos = atof (temp.c_str());
        getline (arquivo, temp, ';');
        avanco = atoi (temp.c_str());
        getline (arquivo, temp, ';');
        juros = atof (temp.c_str());

        if (tipo == "corrente") {  
            float limite;
            Data data;

            getline (arquivo, temp, ';');
            limite = atof (temp.c_str());
            getline (arquivo, temp, '/');
            data.setDia (atoi (temp.c_str ()));
            getline (arquivo, temp, '/');
            data.setMes (atoi (temp.c_str ()));
            getline (arquivo, temp);
            data.setAno (atoi (temp.c_str ()));

            contas.push_back (new ContaCorrente (nome, cpf, idade, limite, juros, data, juros_negativos, juros_positivos, saldo, avanco, tipo));
        }
        else if (tipo == "poupanca") {    
            float saldo_aniversario;
            Data aniversario;

            getline (arquivo, temp, ';');
            saldo_aniversario = atof (temp.c_str());
            getline (arquivo, temp, '/');
            aniversario.setDia (atoi (temp.c_str ()));
            getline (arquivo, temp, '/');
            aniversario.setMes (atoi (temp.c_str ()));
            getline (arquivo, temp);
            aniversario.setAno (atoi (temp.c_str ()));

            contas.push_back (new ContaPoupanca (nome, cpf, idade, aniversario, juros, juros_negativos, juros_positivos, saldo, avanco, tipo, saldo_aniversario));
        }
    }
}
