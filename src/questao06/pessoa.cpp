/**
 * @file	pessoa.cpp
 * @brief	Implementacao dos metodos da classe Pessoa
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	05/06/2017
 * @date	08/06/2017
 * @sa		pessoa.h
 */

#include "pessoa.h"

#include <string>
using std::string;

#include <iostream>
using std::endl;

#include <ostream>
using std::ostream;

#include <sstream>
using std::ostringstream;

/**
 * @details Os valores de cpf e idade sao inicializados com zero, e o nome vazio
 */
Pessoa::Pessoa () {
    nome = "";
    cpf = 0;
    idade = 0;
}

/**
 * @details Os valores dos dados da pessoa sao recebidos por parametro
 * @param   n Nome para a pessoa
 * @param   c Cpf para a pessoa
 * @param   i Idade para a pessoa
 */
Pessoa::Pessoa (string n, long int c, int i) {
    nome = n;
    cpf = c;
    idade = i;
}

/**
 * @return Cpf da pessoa
 */
long int Pessoa::getCpf () {
    return cpf;
}

/**
 * @details O metodo modifica o cpf da pessoa
 * @param   c CPF para a pessoa
 */
void Pessoa::setCpf (long int c) {
    cpf = c;
}

/**
 * @details O metodo modifica o nome da pessoa
 * @param   n Nome para a pessoa
 */
void Pessoa::setNome (string n) {
    nome = n;
}

/**
 * @details O metodo modifica a idade da pessoa
 * @param   i Idade para a pessoa
 */
void Pessoa::setIdade (int i) {
    idade = i;
}


/** 
 * @details O operador e sobrecarregado para representar uma Pessoa
 *			com os dados da pessoa
 * @param	os Referencia para stream de saida
 * @param	p Referencia para o objeto Pessoa a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream &os, Pessoa const &p) {

    os << "Nome: " << p.nome << endl;
    os << "CPF: " << p.cpf << endl;
    os << "Idade: " << p.idade << endl << endl;

    return os;    
}

/** 
 * @details O metodo coloca os dados da pessoa em uma string,
 *          separados por ';'
 * @return	String com os dados da pessoa
 */
string Pessoa::To_string () {
    ostringstream oss;

    oss << nome << ";" << cpf << ";" << idade;

    return oss.str ();
}


/**
 * @details Não ocorre nada no destrutor, pois nao tem variaveis para serem destruidas
 */
Pessoa::~Pessoa () {
    
}