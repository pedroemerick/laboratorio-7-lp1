/**
 * @file	conta_poupanca.cpp
 * @brief	Implementacao dos metodos da classe ContaPoupanca
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	05/06/2017
 * @date	08/06/2017
 * @sa		conta_poupanca.h
 */

#include "conta_poupanca.h"

#include <string>
using std::string;

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

#include <ostream>
using std::ostream;

#include <sstream>
using std::ostringstream;

/**
 * @details Os valores de saldo aniversario é inicializado com zero e o tipo inicializado com poupanca
 */
ContaPoupanca::ContaPoupanca () {
    saldo_aniversario = 0;
    tipo = "poupanca";
}

/**
 * @details Os valores dos dados da conta poupanca sao recebidos por parametro
 * @param   n Nome para o titular
 * @param   c Cpf para o titular
 * @param   i Idade para o titular
 * @param   a Data de aniversario da conta
 * @param   j Juros para a conta
 * @param   jn Juros Negativos para a conta
 * @param   jp Juros Positivos para a conta
 * @param   s Saldo para a conta
 * @param   av Avanco para a data da conta
 * @param   t Tipo da conta
 * @param   sa Saldo no aniversario da conta
 */
ContaPoupanca::ContaPoupanca (string n, long int c, int i, Data a, float j, float jn, float jp, float s, int av, string t, float sa) {
    titular.setNome (n);
    titular.setCpf (c);
    titular.setIdade (i);

    aniversario = a;
    juros = j;
    
    // Sao iniciados como padrao
    juros_positivos = jp;
    juros_negativos = jn;
    saldo = s;
    avanco = av;
    saldo_aniversario = sa;

    tipo = t;
}

/**
 * @details O metodo faz um deposito em conta, pedindo ao usuario 
 *          o valor desejado e somando ao saldo atual
 */
void ContaPoupanca::Deposito () 
{
    float deposito;

    cout << "Digite o valor para deposito: ";
    cin >> deposito;

    if (deposito <= 0)
    {
        cout << endl << "Valor invalido para deposito !!!" << endl;
        return;
    }   

    saldo += deposito;

    cout << endl << "Deposito feito com sucesso !!!" << endl;
}

/**
 * @details O metodo faz um saque na conta, pedindo ao usuario 
 *          o valor desejado e subtraindo ao saldo atual se possivel
 */
void ContaPoupanca::Saque () 
{
    float saque;

    cout << "Digite o valor para saque: ";
    cin >> saque;

    if (saldo < saque || saque <= 0)
    {
        cout << endl << "Valor invalido ou sem saldo para saque !!!" << endl;
        return;
    }   

    saldo -= saque;

    cout << endl << "Saque realizado com sucesso !!!" << endl;
}

/**
 * @return Saldo da conta
 */
float ContaPoupanca::Saldo () {
    return saldo;
}

/**
 * @details O metodo faz o calculo do juros de acordo com o avanco da data
 * @return  Juros de acordo com o avanco de data
 */
float ContaPoupanca::JurosPositivos () {
    
    int temp = saldo_aniversario;
    int temp2 = saldo;

    for (int ii = 0; ii < avanco; ii++)
    {
        juros_positivos = temp * (juros/100);
        temp2 += juros_positivos;
        temp = temp2;
    }
    
    return juros_positivos;
}

/**
 * @return  Juros Negativos
 */
float ContaPoupanca::JurosNegativos () {
    return juros_negativos;
}

/**
 * @details O metodo atualiza o saldo da conta, de acordo o avanco da data e o juros calculado
 */
void ContaPoupanca::Atualiza () {
    
    for (int ii = 0; ii < avanco; ii++)
    {
        juros_positivos = saldo_aniversario * (juros/100);
        saldo += juros_positivos;
        saldo_aniversario = saldo;
    }

    juros_positivos = 0;
    avanco = 0;
}

/**
 * @details O metodo faz o avanco da data atual de acordo o numero de dias passado
 * @param   ad  Quantidade de dias para avancar a data
 */
void ContaPoupanca::AvancaData (int ad) {
    // Ficara apenas em quantos meses tem para avancar
    // pois a poupanca usara o juros de mes em mes
    avanco += ad/30;

    int temp = aniversario.getDia () + ad;

    aniversario.setDia (temp);
    aniversario.OrganizaData ();
}

/**
 * @return  Titular da conta
 */
Pessoa ContaPoupanca::getTitular () {
    return titular;
}

/**
 * @return  Tipo da conta
 */
string ContaPoupanca::getTipo () {
    return tipo;
}

/** 
 * @details O operador e sobrecarregado para representar uma ContaPoupanca
 *			com os dados do conta
 * @param	os Referencia para stream de saida
 * @param	cp Referencia para o objeto ContaPoupanca a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream &os, ContaPoupanca const &cp) {

    os << endl << "Dados do titular:" << endl;
    os << cp.titular;

    os << "Dados da conta:" << endl;
    os << "Tipo: " << cp.tipo << endl;
    os << "Saldo: " << cp.saldo << endl;
    os << "Data de aniversario: " << cp.aniversario << endl;
    os << "Juros Positivos: " << cp.juros_positivos << endl << endl;

    return os;    
}

/** 
 * @details O metodo coloca os dados da conta poupanca em uma string,
 *          separados por ';'
 * @return	String com os dados da conta
 */
string ContaPoupanca::To_string () {
    ostringstream oss;

    oss << tipo << ";" << titular.To_string () << ";" << saldo << ";" << juros_positivos << ";";
    oss << juros_negativos << ";" << avanco << ";" << juros << ";" << saldo_aniversario << ";" << aniversario << endl;

    return oss.str ();
}

/**
 * @details Não ocorre nada no destrutor, pois nao tem variaveis para serem destruidas
 */
ContaPoupanca::~ContaPoupanca () {
    
}