/**
 * @file	conta_corrente.cpp
 * @brief	Implementacao dos metodos da classe ContaCorrente
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	05/06/2017
 * @date	08/06/2017
 * @sa		conta_corrente.h
 */

#include "conta_corrente.h"
#include "data.h"

#include <string>
using std::string;

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

#include <ostream>
using std::ostream;

#include <sstream>
using std::ostringstream;


/**
 * @details Os valores de limite é inicializado com zero e o tipo inicializado com corrente
 */
ContaCorrente::ContaCorrente () {
    limite = 0;
    tipo = "corrente";
}

/**
 * @details Os valores dos dados da conta corrente sao recebidos por parametro
 * @param   n Nome para o titular
 * @param   c Cpf para o titular
 * @param   i Idade para o titular
 * @param   l Limite para a conta
 * @param   j Juros para a conta
 * @param   cr Data da conta
 * @param   jn Juros Negativos para a conta
 * @param   jp Juros Positivos para a conta
 * @param   s Saldo para a conta
 * @param   av Avanco para a data da conta
 * @param   t Tipo da conta
 */
ContaCorrente::ContaCorrente (string n, long int c, int i, float l, float j, Data cr, float jn, float jp, float s, int av, string t) {
    titular.setNome (n);
    titular.setCpf (c);
    titular.setIdade (i);

    data = cr;
    limite = l;
    juros = j;

    juros_negativos = jp;
    juros_positivos = jn;
    saldo = s;
    avanco = av;

    tipo = t;
}

/**
 * @details O metodo faz um deposito em conta, pedindo ao usuario 
 *          o valor desejado e somando ao saldo atual
 */
void ContaCorrente::Deposito () 
{
    float deposito;

    cout << "Digite o valor para deposito: ";
    cin >> deposito;

    if (deposito <= 0)
    {
        cout << endl << "Valor invalido para deposito !!!" << endl;
        return;
    }   

    saldo += deposito;

    cout << endl << "Deposito realizado com sucesso !!!" << endl;
}

/**
 * @details O metodo faz um saque na conta, pedindo ao usuario 
 *          o valor desejado e subtraindo ao saldo atual se possivel
 */
void ContaCorrente::Saque () 
{
    float saque;

    cout << "Digite o valor para saque: ";
    cin >> saque;

    if ((saldo - saque) < limite || saque <= 0)
    {
        cout << endl << "Valor invalido ou sem saldo para saque !!!" << endl;
        return;
    }   

    saldo -= saque;

    cout << endl << "Saque realizado com sucesso !!!" << endl;
}

/**
 * @return Saldo da conta
 */
float ContaCorrente::Saldo () {
    return saldo;
}

/**
 * @return  Juros Positivos
 */
float ContaCorrente::JurosPositivos () {
    return juros_positivos;
}

/**
 * @details O metodo faz o calculo do juros de acordo com o avanco da data
 * @return  Juros de acordo com o avanco de data
 */
float ContaCorrente::JurosNegativos () {

    int temp = saldo;
    if (saldo > 0)
        return juros_negativos;

    for (int ii = 0; ii < avanco; ii++)
    {
        juros_negativos = temp * (juros/100);
        temp += juros_negativos;
    }
    
    return juros_negativos;
}

/**
 * @details O metodo atualiza o saldo da conta, de acordo o avanco da data e o juros calculado
 */
void ContaCorrente::Atualiza () {

    for (int ii = 0; ii < avanco; ii++)
    {
        juros_negativos = saldo * (juros/100);
        saldo += juros_negativos;
    }

    juros_negativos = 0;
    avanco = 0;
}

/**
 * @details O metodo faz o avanco da data atual de acordo o numero de dias passado
 * @param   ad  Quantidade de dias para avancar a data
 */
void ContaCorrente::AvancaData (int ad) {
    avanco += ad;

    int temp = data.getDia () + avanco;

    data.setDia (temp);
    data.OrganizaData ();
}

/**
 * @return  Titular da conta
 */
Pessoa ContaCorrente::getTitular () {
    return titular;
}

/**
 * @return  Tipo da conta
 */
string ContaCorrente::getTipo () {
    return tipo;
}

/** 
 * @details O operador e sobrecarregado para representar uma ContaCorrente
 *			com os dados da conta
 * @param	os Referencia para stream de saida
 * @param	cc Referencia para o objeto ContaCorrente a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream &os, ContaCorrente const &cc ) {

    os << endl << "Dados do titular:" << endl;
    os << cc.titular;

    os << "Dados da conta:" << endl;
    os << "Tipo: " << cc.tipo << endl;
    os << "Saldo: " << cc.saldo << endl;
    os << "Limite: " << cc.limite << endl;
    os << "Data: " << cc.data << endl;
    os << "Juros Negativos: " << cc.juros_negativos << endl << endl;

    return os;    
}

/** 
 * @details O metodo coloca os dados da conta corrente em uma string,
 *          separados por ';'
 * @return	String com os dados da conta
 */
string ContaCorrente::To_string () {
    ostringstream oss;

    oss << tipo << ";" << titular.To_string () << ";" << saldo << ";" << juros_positivos << ";";
    oss << juros_negativos << ";" << avanco << ";" << juros << ";" << limite << ";" << data << endl;

    return oss.str ();
}

/**
 * @details Não ocorre nada no destrutor, pois nao tem variaveis para serem destruidas
 */
ContaCorrente::~ContaCorrente () {

}