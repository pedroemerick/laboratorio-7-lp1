/**
* @file     funcoes_q6.cpp
* @brief    Implementacao da funcao que realiza a abertura de uma conta, fecha uma conta,
*           verifica se uma conta existe, faz acesso a uma conta, e retorna uma conta da lista
* @author   Pedro Emerick (p.emerick@live.com)
* @since    05/06/2017
* @date	    08/06/2017
*/

#include "funcoes_q6.h"
#include "conta_corrente.h"
#include "conta_poupanca.h"
#include "data.h"

#include <list>
using std::list;

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <string>
using std::string;

#include <sstream>
using std::ws;

/** 
 * @brief	Função que verifica se existe uma conta na lista de contas atraves do cpf
 * @param 	contas Lista com as contas
 * @param	cpf Cpf do titular da conta
 * @return  Se a conta existe ou nao na lista
 */
bool existe_conta (list <Conta*> &contas, long int cpf) 
{   
    for (auto it = contas.begin (); it != contas.end (); it++)
    {
        if (((*it)->getTitular ()).getCpf () == cpf)
            return true;
    }

    return false;
}

/** 
 * @brief	Função que retorna uma conta da lista de contas atraves do cpf
 * @param 	contas Lista com as contas
 * @param	cpf Cpf do titular da conta
 * @return  Iterador para a conta desejada
 */
list<Conta*>::iterator retorna_conta (list <Conta*> &contas, long int cpf) 
{
    list<Conta*>::iterator it;

    for (it = contas.begin (); it != contas.end (); it++)
    {
        if (((*it)->getTitular ()).getCpf () == cpf)
            return it;
    }

    return it;
}

/** 
 * @brief	Função que faz a abertura de uma conta, recebendo os dados necessarios do usuario e 
 *          inserindo na lista de contas
 * @param 	contas Lista com as contas
 * @param	opcao Opcao de qual tipo de conta sera aberta, se sera corrente ou poupanca
 * @param   qtd_contas Quantidade de contas
 */
void abrir_conta (list <Conta*> &contas, int opcao, int &qtd_contas)
{
    string nome;
    long int cpf;
    int idade;

    cout << "Nome do titular: ";
    getline (cin >> ws, nome);

    cout << "CPF: ";
    cin >> cpf;

    if (existe_conta (contas, cpf) == true)
    {
        cout << endl << "Conta já aberta !!!" << endl;
        return;
    }

    cout << "Idade: ";
    cin >> idade;

    Data hoje;

    if (opcao == 1)
    {
        int aux = -1;
        float limite;
        while (aux != 0)
        {
            cout << "Limite de credito: ";
            cin >> limite;

            if (limite > 0)
                cout << endl << "O limite deve ser um valor negativo !!!" << endl << endl;
            else 
                aux = 0;
        }

        float juros;
        cout << "Juros: ";
        cin >> juros;

        contas.push_back (new ContaCorrente (nome, cpf, idade, limite, juros, hoje, 0, 0, 0, 0, "corrente"));
        qtd_contas += 1;
    }
    else
    {
        float juros;
        cout << "Juros: ";
        cin >> juros;

        contas.push_back (new ContaPoupanca (nome, cpf, idade, hoje, juros, 0, 0, 0, 0, "poupanca", 0));
        qtd_contas += 1;
    }

    cout << endl << "Conta aberta com sucesso !!!" << endl;
}

/** 
 * @brief	Função que faz acesso a uma conta e faz as alteracoes possiveis e desejadas pelo usuario
 * @param 	contas Lista com as contas
 */
void acesso_conta (list <Conta*> &contas) 
{
    long int cpf;

    cout << "Digite seu cpf: ";
    cin >> cpf;

    if (existe_conta (contas, cpf) == false)
    {
        cout << endl << "Conta nao encontrada !!!" << endl;
        return;
    }

    auto aux = retorna_conta (contas, cpf);

    if ((*aux)->getTipo () == "corrente")
    {
        ContaCorrente *conta_aux = dynamic_cast <ContaCorrente*> (*aux);

        int opcao_conta = -1;

        while (opcao_conta != 0)
        {
            opcao_conta = menu_conta ();

            switch (opcao_conta)
            {
                case 1:
                    conta_aux->Deposito ();
                    break;
                case 2:
                    conta_aux->Saque ();
                    break;
                case 3:
                    cout << "Saldo atual: R$ " << conta_aux->Saldo () << endl;
                    break;
                case 4:
                    conta_aux->Atualiza ();
                    cout << "Saldo atualizado com sucesso !!!" << endl;
                    break;
                case 5: {
                    int avanco;
                    cout << "Quantos dias deseja avancar: ";
                    cin >> avanco;
                    conta_aux->AvancaData (avanco);
                    break;
                }
                case 6:
                     cout << "Juros apos avanco de data: R$ " << conta_aux->JurosNegativos () << endl;
                     break;
                case 7:
                    cout << *conta_aux;
                    break;
                case 0:
                    break;
                default:
                    cout << "Opcao Invalida !!!" << endl;
                    break;  
            }
        }
    }
    else 
    {
        ContaPoupanca *conta_aux = dynamic_cast <ContaPoupanca*> (*aux);

        int opcao_conta = -1;

        while (opcao_conta != 0)
        {
            opcao_conta = menu_conta ();

            switch (opcao_conta)
            {
                case 1:
                    conta_aux->Deposito ();
                    break;
                case 2:
                    conta_aux->Saque ();
                    break;
                case 3:
                    cout << "Saldo atual: R$ " << conta_aux->Saldo () << endl;
                    break;
                case 4:
                    conta_aux->Atualiza ();
                    cout << "Saldo atualizado com sucesso !!!" << endl;
                    break;
                case 5: {
                    int avanco;
                    cout << "Quantos dias deseja avancar: ";
                    cin >> avanco;
                    conta_aux->AvancaData (avanco);
                    break;
                }
                case 6:
                     cout << "Juros apos avanco de data: R$ " << conta_aux->JurosPositivos () << endl;
                     break;
                case 7:
                    cout << *conta_aux;
                    break;
                case 0:
                    break;
                default:
                    cout << "Opcao Invalida !!!" << endl;
                    break;  
            }
        }
    }
}

/** 
 * @brief	Função que faz o fechamento de uma conta removendo-o da lista
 * @param 	contas Lista com as contas
 * @param   qtd_contas Quantidade de contas
 */
void remove_conta (list <Conta*> &contas, int &qtd_contas)
{
    long int cpf;

    cout << "Digite seu cpf para fechar sua conta: ";
    cin >> cpf;

    if (existe_conta (contas, cpf) == false)
    {
        cout << endl << "Conta nao encontrada !!!" << endl;
        return;
    }

    auto aux = retorna_conta (contas, cpf);

    contas.remove (*aux);
    qtd_contas -= 1;

    cout << endl <<     "Conta fechada com sucesso !!!" << endl;
}