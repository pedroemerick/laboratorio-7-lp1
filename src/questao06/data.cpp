/**
 * @file	data.cpp
 * @brief	Implementacao dos metodos da classe Data
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	30/05/2017
 * @date	08/06/2017
 * @sa		data.h
 */

#include "data.h"
#include "menus.h"

#include <ostream>
using std::ostream;

#include <istream>
using std::istream;

#include <sstream>
using std::ostringstream;

#include <iostream>
using std::cout;
using std::endl;

/**
 * @details Os valores de dia, mes e ano sao inicializados com o dia, mes e ano atual
 */
Data::Data () {
    struct tm *horario_local;
    time_t t;
            
    t = time(NULL);                     // Obtem informações de data e hora
    horario_local = localtime(&t);      // Converte a hora atual para a hora local

    dia = horario_local->tm_mday;
    mes = horario_local->tm_mon + 1;
    ano = horario_local->tm_year + 1900;
}

/**
 * @details Os valores dos dados da data sao recebidos por parametro
 * @param   d Dia para a data
 * @param   m Mes para a data
 * @param   a Ano para a data
 */
Data::Data (int d, int m, int a) {
    dia = d;
    mes = m;
    ano = a;
}

/**
 * @return Dia da data
 */
int Data::getDia () {
    return dia;
}

/**
 * @details O metodo modifica o dia da data
 * @param   d Dia para a data
 */
void Data::setDia (int d) {
    dia = d;
}

/**
 * @return Mes da data
 */
int Data::getMes () {
    return mes;
}

/**
 * @details O metodo modifica o mes da data
 * @param   m Mes para a data
 */
void Data::setMes (int m) {
    mes = m;
}

/**
 * @return Ano da data
 */
int Data::getAno () {
    return ano;
}

/**
 * @details O metodo modifica o ano da data
 * @param   a Ano para a data
 */
void Data::setAno (int a){
    ano = a;
}

/** 
 * @details O operador e sobrecarregado para representar uma Data
 *			com os dados da Data
 * @param	os Referencia para stream de saida
 * @param	d Referencia para o objeto Data a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream& os, const Data& d) {
    return os << d.dia << "/" << d.mes << "/" << d.ano;
}

/** 
 * @details O operador e sobrecarregado para receber os dados uma Data
 * @param	i Referencia para stream de entrada
 * @param	d Referencia para o objeto Data a receber os dados
 * @return	Referencia para stream de entrada
 */
istream& operator >> (istream& i, Data &d) {
    i >> d.dia >> d.mes >> d.ano;
    return i;
}

/** 
 * @details O metodo coloca os dados da data em uma string,
 *          separados por ';'
 * @return	String com os dados da data
 */
string Data::To_string () {
    ostringstream oss;

    oss << dia << "/" << mes << "/" << ano;

    return oss.str ();
}

/** 
 * @details O metodo organiza a data para os dias e meses nao passarem seu valor do limite, 
 *          que sao os dias com no maximo 30 e os meses com no maximo 12
 */
void Data::OrganizaData () 
{
    if (mes > 12) {
        ano += mes/12;
        mes -= (mes/12) * 12;

        if (mes == 0)
            mes = 1;
    }
    else if (dia > 30) {
        mes += dia/30;
        dia -= (dia/30) * 30;

        if (dia == 0)
            dia = 1;
    }
}

/** 
 * @details O operador e sobrecarregado para realizar a verificacao de igualdade
 *			de um instante de Data a outro
 * @param	d Instante de Data que sera comparado ao objeto que invoca o metodo
 * @return	Se um instante é igual ao outro
 */
bool Data::operator == (Data const d) {
    if (dia == d.dia && mes == d.mes && ano == d.ano) {
        return true;
}

    return false;
}

/**
 * @details Não ocorre nada no destrutor, pois nao tem variaveis para serem destruidas
 */
Data::~Data () {

}