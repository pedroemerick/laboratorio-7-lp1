/**
* @file     menus.cpp
* @brief    Implementacao das funcoes que imprimem os menus do programa e recebe a opcao do usuario
* @author   Pedro Emerick (p.emerick@live.com)
* @since    11/05/2017
* @date	    08/06/2017
*/

#include "menus.h"

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

/**
* @brief Função que imprime o menu principal com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_principal ()
{
    int menu_principal;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                   Menu Principal                  *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Abrir Conta Corrente                      *" << endl;
    cout << "*      2) Abrir Conta Poupança                      *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      3) Acessar Conta                             *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      4) Fechar Conta                              *" << endl;    
    cout << "*                                                   *" << endl;
    cout << "*      0) Sair                                      *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opcao que condiz com seu tipo de usuario:" << endl << "--> ";
    cin >> menu_principal;
    cout << endl;

    return menu_principal;
}

/**
* @brief Função que imprime o menu das contas com suas opcoes e recebe a escolha do usuario
* @return Opcao escolhida pelo usuario
*/
int menu_conta ()
{
    int menu_conta;

    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                    Ação Desejada                  *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Deposito                                  *" << endl;
    cout << "*      2) Saque                                     *" << endl;  
    cout << "*      3) Saldo                                     *" << endl;
    cout << "*      4) Atualizar Saldo                           *" << endl;
    cout << "*      5) Avancar Data                              *" << endl;
    cout << "*      6) Ver Juros Apos Avanco de Data             *" << endl;
    cout << "*      7) Visualizar Dados da Conta                 *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Voltar                                    *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opcao desejada:" << endl << "--> ";
    cin >> menu_conta;
    cout << endl;

    return menu_conta;
}