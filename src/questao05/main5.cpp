/**
* @file	    main5.cpp
* @brief	Arquivo com a função principal do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since	08/06/2017
* @date	    08/06/2017
*/

/*  O proposito deste programa, é pegar um conjunto de numeros e imprimir o quadrado de cada um,
para isto são utilizados dois vector um armazenando os valores que quero saber o quadrado,
e o outro com o quadrado deles recebendo o valor do quadrado por uma funcao. 

    Para colocar os valores no vector é usada a funcao transform que pega todos os valores do vector 
com os numeros, chamando a funcao do quadrado para cada um deles, 
inserindo o retorno da funcao do quadrado no vector dos quadrados.
*/

#include <iostream>
using std::cout;
using std::endl;

#include <iterator>
using std::back_inserter;

#include <vector>
using std::vector;

#include <algorithm>
using std::transform;

/** 
 * @brief	Função que retorna o quadrado de um numero inteiro
 * @param 	val Numero para retorno do seu quadrado
 * @return  Numero passado ao quadrado
 */
int square(int val)
{
    return val * val;
}

/** 
 * @brief	Função principal do programa
 */
int main(int argc, char *argv[])
{
    /* Sao iniciados dois vector */
    vector<int> col;
    vector<int> col2;

    /* È inserido os numeros de 1 a 9 no vector col, inserindo sempre no fim do vector */
    for (int i = 1; i <= 9; ++i)
    {
        col.push_back(i);
    }

    /* Chama a funcao square para todos os valores do vector col, indo do inicio ao fim do vector,
    e inserindo no vector col2 o retorno da funcao square toda vez que chamada,
    ficando assim o vector col2 com todos valores ao quadrado do vector col
    */
    transform(col.begin(), col.end(), back_inserter(col2), square);

    /* Percorre o vector col2 utilizando um iterador, do inicio ao fim, 
    imprimindo seus valores separados por um espaço
    */
    for (vector<int>::iterator it = col2.begin(); it != col2.end(); ++it)
    {
        cout << (*it) << " ";
    }

    cout << endl;

    return 0;
}