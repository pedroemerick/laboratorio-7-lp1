/**
* @file	    main4.cpp
* @brief	Arquivo com a função principal do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since	05/06/2017
* @date	    07/06/2017
*/

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;
using std::to_string;

#include <set>
using std::set;

#include <vector>
using std::vector;

#include <algorithm>
using std::find_if;

/** 
 * @brief	Função que verifica se um numero é primo
 * @param 	x Numero para verificacao
 * @return  Se o numero é primo ou nao
 */
bool verifica_primo (int x)  
{
    int divisor = 2;

    if (x == 1)
        return false;

    while (divisor <= x/2)
    {
        if (x % divisor == 0)
            return false;
        
        divisor += 1;
    }

    return true;
}

/**
* @brief Função principal do programa
*/
int main (int argc, char *argv [])
{
    vector <int> numeros;

    int n = atoi (argv [1]);

    for (int ii = 1; ii <= n; ii++)
    {
        numeros.push_back (ii);
    }

    set <int> primos;

    cout << "Numeros primos [1-" << n << "]: ";

    // Busca o sempre o proximo numero primo e imprime
    for (auto it = find_if (numeros.begin(), numeros.end (), verifica_primo); it != numeros.end (); ) {
        cout << *it << " ";
        it++;
        it = find_if (it, numeros.end (), verifica_primo);
    }

    cout << endl;

    return 0;
}